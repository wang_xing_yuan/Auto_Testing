# Auto_Testing

#### 介绍
自动化压力测试；利用jmeter进行单接口接口自动摸底、循环测试，寻找服务器最优性能数据
获得接口相关性能指标数据，并且可视化



#### 依赖包

1. matplotlib
2. paramiko
3. wxy-Mail
4. wxy_MySQL
5. wxy-csv



#### 软件原理
1. 调用os模块执行jmeter脚本
2. 处理jmeter测试生成csv文件得到测试数据
3. 利用pustil进行服务器性能指标监控（CPU、内存等）
4. 利用matplotlib进行测试数据可视化
5. 利用paramiko建立ssh连接远程linux服务器
6. 测试数据存放MySQL，需要可导出


#### 使用方法
1.使用示例
```python
from core.Auto import AUTO

AUTO.SetUp('本次测试名称（str）',
           ['jmeter脚本路径1',
            'jmeter脚本路径2',],
           '测试数据存储路径(str)',
           'jmeter脚本执行时间(int)'
           )                                                  # 输入示例中4个参数

AUTO.StartTest()                                              # 开始执行

```

2.结果示例
![](.README_images/1.png)
![](.README_images/2.png)

