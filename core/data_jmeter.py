from wxy_csv import CSV


def jmeter_data(csv_path):
    '''
    平均响应时间
    95%line(最大值)
    错误率
    TPS
    '''

    # 读取csv文件内容,拿到第2、8列数据，分别为响应时间、执行时间、响应成功与否
    read_file = CSV.select(csv_path, '列', [2, 8, 1])

    # 计算响应时间
    elapsed = [int(i) for i in read_file[0]]  # csv中读取的数据全部为字符串，需要转为int才可使用
    average_time = sum(elapsed) / len(elapsed)  # 平均响应时间时间
    elapsed.sort()
    # min_time = elapsed[0]
    # max_time = elapsed[-1]
    p95_time = elapsed[int(0.95 * len(elapsed))]  # 95%line(做最大响应时间)
    # print('average:', average_time)
    # print('95%line:', p95_time)

    # 计算错误率
    success = read_file[1]
    false = success.count('false')
    error = (false * 100) / len(success)  # 错误率
    error = round(error,1)
    # print('error:', error, '%')

    # 计算TPS
    duration = [int(i) for i in read_file[2]]
    durations = (duration[-1] - duration[0]) / 1000  # jmeter脚本运行时间
    sample = len(duration)
    TPS = (sample - false) / durations  # TPS
    TPS = round(TPS, 1)
    # print('TPS:', TPS)


    return [average_time,p95_time,error,TPS]