from core.Setting import db_win
from wxy_MySQL import MySQLdb
from pylab import *
import os


mpl.rcParams['font.sans-serif'] = ['SimHei']

def data(db,table,condition,save_path):
    marker_size=0

    # 查询条件
    threads = [float(i) for i in db.select(table, 'num_threads', condition)]  # 线程数

    avg_time = [float(i) for i in db.select(table, 'avg_res_time', condition)]  # 平均响应时间
    # max_time = [float(i) for i in db.select(table, 'p95_time', condition)]  # 最大响应时间
    tps = [float(i) for i in db.select(table, 'TPS', condition)]  # TPS
    web_cpu = [float(i) for i in db.select(table, 'web_cpu', condition)]  # web服务器cpu
    web_memory = [float(i) for i in db.select(table, 'web_memory', condition)]  # web服务器memory
    web_tcp = [float(i) for i in db.select(table, 'web_tcp', condition)]  # web服务器TCP
    mongo_cpu = [float(i) for i in db.select(table, 'mongo_cpu', condition)]  # mongo服务器cpu
    mongo_memory = [float(i) for i in db.select(table, 'mongo_memory', condition)]  # mongo服务器memory
    mongo_tcp = [float(i) for i in db.select(table, 'mongo_tcp', condition)]  # mongo服务器TCP
    redis_cpu = [float(i) for i in db.select(table, 'redis_cpu', condition)]  # redis服务器cpu
    redis_memory = [float(i) for i in db.select(table, 'redis_memory', condition)]  # redis服务器memory
    redis_tcp = [float(i) for i in db.select(table, 'redis_tcp', condition)]  # redis服务器TCP

    def AVERAGE_TIME():
        '''平均响应时间'''

        plt.figure(figsize=(10, 5))
        plt.plot(threads, avg_time, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.plot(threads,[1000 for i in range(len(threads))], marker='o', markersize=marker_size, markerfacecolor='red', c='red')
        plt.legend(['平均响应时间','标准'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('平均响应时间(ms)')
        plt.title('接口平均响应时间')
        plt.savefig(save_path + '\\avg_res_time.png')
        # plt.show()
    # def m_time():
    #
    #     plt.figure(figsize=(10, 5))
    #     plt.plot(threads, max_time, marker='o', markersize=1, markerfacecolor='brown', c='brown')
    #     plt.legend(['最大响应时间'], loc=2)
    #     plt.xlabel('并发线程数')
    #     plt.ylabel('最大响应时间(ms)')
    #     plt.title('接口最大响应时间')
    #     # plt.savefig(save_path + '\\max_res_time.png')
    #     plt.show()
    def TPS():
        '''TPS'''

        plt.figure(figsize=(10, 5))
        plt.plot(threads, tps, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.ylim((0,max(tps)+50))
        plt.legend(['TPS'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('TPS')
        plt.title('TPS')
        plt.savefig(save_path + '\\TPS.png')
        # plt.show()
    def WEB_CPU():
        '''web服务器CPU'''

        plt.figure(figsize=(10, 5))
        plt.plot(threads, web_cpu, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.plot(threads,[80 for i in threads], marker='o', markersize=marker_size, markerfacecolor='red', c='red')
        plt.ylim((0, 105))
        plt.legend(['web服务器cpu利用率','标准'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('web服务器cpu利用率(%)')
        plt.title('web服务器cpu利用率')
        plt.savefig(save_path + '\\Web_CPU.png')
        # plt.show()
    def WEB_MEMORY():

        plt.figure(figsize=(10, 5))
        plt.plot(threads, web_memory, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.plot(threads,[80 for i in threads], marker='o', markersize=marker_size, markerfacecolor='red', c='red')
        plt.ylim((0, 105))
        plt.legend(['web服务器内存占用','标准'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('web服务器内存占用(%)')
        plt.title('web服务器内存占用')
        plt.savefig(save_path + '\\Web_Memory.png')
        # plt.show()
    def WEB_TCP():

        plt.figure(figsize=(10, 5))
        plt.plot(threads, web_tcp, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.ylim((0, max(web_tcp)+300))
        plt.legend(['TCP连接数'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('web服务器TCP连接数')
        plt.title('web服务器TCP连接数')
        plt.savefig(save_path + '\\Web_Tcp.png')
        # plt.show()
    def MONGO_CPU():

        plt.figure(figsize=(10, 5))
        plt.plot(threads, mongo_cpu, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.plot(threads, [80 for i in threads], marker='o', markersize=marker_size, markerfacecolor='red', c='red')
        plt.ylim((0, 105))
        plt.legend(['mongo服务器cpu利用率','标准'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('mongo服务器cpu利用率(%)')
        plt.title('mongo服务器cpu利用率')
        plt.savefig(save_path + '\\Mongo_CPU.png')
        # plt.show()
    def MONGO_MEMORY():

        plt.figure(figsize=(10, 5))
        plt.plot(threads, mongo_memory, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.plot(threads, [80 for i in threads], marker='o', markersize=marker_size, markerfacecolor='red', c='red')
        plt.ylim((0, 105))
        plt.legend(['mongo服务器内存占用','标准'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('mongo服务器内存占用(%)')
        plt.title('mongo服务器内存占用')
        plt.savefig(save_path + '\\Mongo_Memory.png')
        # plt.show()
    def MONGO_TCP():
        plt.figure(figsize=(10, 5))
        plt.plot(threads, mongo_tcp, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.ylim((0, max(mongo_tcp) + 300))
        plt.legend(['TCP连接数'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('mongo服务器TCP连接数')
        plt.title('mongo服务器TCP连接数')
        plt.savefig(save_path + '\\Mongo_Tcp.png')
        # plt.show()
    def REDIS_CPU():

        plt.figure(figsize=(10, 5))
        plt.plot(threads, redis_cpu, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.plot(threads, [80 for i in threads], marker='o', markersize=marker_size, markerfacecolor='red', c='red')
        plt.ylim((0, 105))
        plt.legend(['redis服务器cpu利用率','标准'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('redis服务器cpu利用率(%)')
        plt.title('redis服务器cpu利用率')
        plt.savefig(save_path + '\\Redis_CPU.png')
        # plt.show()
    def REDIS_MEMORY():

        plt.figure(figsize=(10, 5))
        plt.plot(threads, redis_memory, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.plot(threads, [80 for i in threads], marker='o', markersize=marker_size, markerfacecolor='red', c='red')
        plt.ylim((0, 105))
        plt.legend(['redis服务器cpu占用','标准'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('redis服务器内存占用(%)')
        plt.title('redis服务器内存占用')
        plt.savefig(save_path + '\\Redis_Memory.png')
        # plt.show()
    def REDIS_TCP():
        plt.figure(figsize=(10, 5))
        plt.plot(threads, redis_tcp, marker='o', markersize=marker_size, markerfacecolor='brown', c='brown')
        plt.ylim((0, max(redis_tcp) + 300))
        plt.legend(['TCP连接数'], loc=2)
        plt.xlabel('并发线程数')
        plt.ylabel('redis服务器TCP连接数')
        plt.title('redis服务器TCP连接数')
        plt.savefig(save_path + '\\Redis_Tcp.png')
        # plt.show()

    AVERAGE_TIME()
    # m_time()
    TPS()
    WEB_CPU()
    WEB_MEMORY()
    WEB_TCP()
    MONGO_CPU()
    MONGO_MEMORY()
    MONGO_TCP()
    REDIS_CPU()
    REDIS_MEMORY()
    REDIS_TCP()



if __name__ == '__main__':
    db = MySQLdb(db_win['host'],
                 db_win['user'],
                 db_win['password'],
                 'performance_testing')
    table_name='d_20190430_t09_test_data'
    condition='I_name="app拉课程详情-新增.jmx"'
    data(db,table_name,condition,os.path.dirname(__file__))
