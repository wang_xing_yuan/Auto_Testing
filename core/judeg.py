




def judge(db,data_table,conditions):
    '''
    根据测试数据判断该接口是否还要继续执行
    传入sql查询条件
    1.cpu占用超过90%
    2.memory占用超过90%
    3.错误率大于3%
    4.平均响应时间大于1500ms
    5.最大响应时间大于5000ms
    满足上面任意一条，该接口测试结束
    '''

    error=db.select(data_table,'error',conditions,sql=False)[0]
    # print(type(error))
    avg_res_time=db.select(data_table,'avg_res_time',conditions,sql=False)[0]
    # print(type(avg_res_time))
    p95_time=db.select(data_table,'p95_time',conditions,sql=False)[0]
    # print(type(p95_time))
    # cpu=db.select(data_table,'web_cpu',conditions,sql=False)[0]
    # print(type(cpu))
    # memory=db.select(data_table,'web_memory',conditions,sql=True)[0]
    # print(type(memory))

    if error>3 or avg_res_time>1500 or p95_time>5000:
        return False
    else:
        return True