from core.Setting import mail
from core.Auto import AUTO
from wxy_Mail import Mail
from core.Linux import close_monitor
import traceback
import time
import os



if __name__ == '__main__':
    AUTO.SetUp(
        '直播课堂5.9web测试',
        ["D:\Python\\fulaan\LIVE\压测1\jmeter_script\web1-getMyAllBuyExcellentCourses.do.jmx",
         "D:\Python\\fulaan\LIVE\压测1\jmeter_script\web2-getMyOwnDetails.do.jmx",
         "D:\Python\\fulaan\LIVE\压测1\jmeter_script\web3-gotoClass.do.jmx",
         "D:\Python\\fulaan\LIVE\压测1\jmeter_script\web4-booleanBackList.do.jmx",
         "D:\Python\\fulaan\LIVE\压测1\jmeter_script\web5-booleanUserAgreement.do.jmx",
         "D:\Python\\fulaan\LIVE\压测1\jmeter_script\web6-getAllRole.do.jmx",
         "D:\Python\\fulaan\LIVE\压测1\jmeter_script\web7-myRoleCommunitys.do.jmx"],
        "D:\Python\\auto",
        600,
               )
    try:
        t1 = time.time()
        AUTO.StartTest()
        t2 = time.time()

        mail = Mail(mail['user'], mail['password'])
        mail.add_subject('测试完成')
        mail.add_receivers(['490347953@qq.com'])
        content = '测试完成，共耗时{}h'.format(round((t2 - t1) / 3600, 2))
        mail.add_content('plain', content)
        mail.send()

        # os.popen('shutdown -s')

    except Exception as e:
        error=traceback.format_exc()
        mail = Mail(mail['user'], mail['password'])
        mail.add_subject('测试终止')
        mail.add_receivers(['490347953@qq.com'])
        mail_fpath=os.path.dirname(__file__)+'\mail\\test_fail.html'
        with open(mail_fpath,'r',encoding='utf-8') as f:
            content=f.read()
        content=content.format(error).replace('\n','<br>')
        mail.add_content('html', content)
        mail.send()


        close_monitor()


        # os.popen('shutdown -s')